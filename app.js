"use strict";
const express = require("express");
const requestHandlers = require("./scripts/request-handlers.js");
const bodyParser = require("body-parser");
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("www"));

// Tornaments
app.route("/tornaments")
.get(requestHandlers.getTornaments);

app.route("/tornament")
.post(requestHandlers.createUpdateTornament);

app.route("/start_tornament/:id")
.put(requestHandlers.startTornament);

app.route("/tornament/:id")
.put(requestHandlers.createUpdateTornament);

// Modalities
app.route("/modalities")
.get(requestHandlers.getTornamentTypes);

//players
app.route("/players")
.get(requestHandlers.getPlayers);

app.route("/player")
.post(requestHandlers.createUpdatePlayer);

app.route("/player/:id")
.put(requestHandlers.createUpdatePlayer);


// Teams
app.route("/team/:tornamentTeamId")
.post(requestHandlers.createUpdateTeam)
.put(requestHandlers.createUpdateTeam);

app.route("/teams")
.get(requestHandlers.getTeams);

//games
//roundRobin
app.route("/games/:tornamentID")
.post(requestHandlers.createTornamentGames);

app.route("/games")
.get(requestHandlers.getGames);

app.route("/games/:gameID")
.put(requestHandlers.setGameResult);


//deletes
app.route("/tornament/:tornamentID")
    .delete(requestHandlers.deleteTornament);

app.route("/teams/:teamID")
    .delete(requestHandlers.deleteTeam);

app.route("/games/:tournamentID")
    .delete(requestHandlers.deleteGames);

app.route("/players/:playerID")
    .delete(requestHandlers.deletePlayer);

app.listen(8085, function() {
    console.log("Server running at http://localhost:8085");
});