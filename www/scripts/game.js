"use strict";

/** 
 * @class Estrutura com capacidade de armazenar o estado de uma entidade jogo 
 * @constructs Equipa
 * @param {int} id - id da equipa
 * @param {Text} name - nome da equipa
 */
function Jogo(id, idEquipa1, idEquipa2, golos1, golos2, idTorneio, nomeEquipa1, nomeEquipa2) {
    this.id = id;
    this.idEquipa1 = idEquipa1;
    this.idEquipa2 = idEquipa2;
    this.golos1 = golos1;
    this.golos2 = golos2;
    this.idTorneio = idTorneio;
    this.nomeEquipa1 = nomeEquipa1;
    this.nomeEquipa2 = nomeEquipa2;
};