"use strict";

/** 
 * @class Estrutura com capacidade de armazenar o estado de uma entidade torneio 
 * @constructs Torneio
 * @param {int} id - id do torneio
 * @param {Text} name - nome do torneio
 * @param {Date} data - data do torneio
 * @param {Time} hora - hora do torneio
 * @param {Text} local - local do torneio
 * @param {Text} tipo - tipo do torneio
 */
function Torneio(id, nome, modalidade, data, hora, local, tipo, started /*equipas */) {
    this.id = id;
    this.nome = nome;
    this.modalidade = modalidade;
    this.tipo = tipo;
    this.local = local;
    this.hora = hora;
    this.data = data;
    this.equipas = [];
    this.jogos = [];
    this.started = started;
};