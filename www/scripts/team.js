"use strict";

/** 
 * @class Estrutura com capacidade de armazenar o estado de uma entidade equipa 
 * @constructs Equipa
 * @param {int} id - id da equipa
 * @param {Text} name - nome da equipa
 */
function Equipa(id, nome, idTorneio, pontos) {
    this.id = id;
    this.nome = nome;
    this.jogadores = [];
    this.pontos = pontos;
    this.idTorneio = idTorneio;
};