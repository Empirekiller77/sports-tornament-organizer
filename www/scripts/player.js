"use strict";

/** 
 * @class Estrutura com capacidade de armazenar o estado de uma entidade jogador 
 * @constructs Jogador
 * @param {int} id - id do jogador
 * @param {text} name - nome do jogador
 * @param {Text} password - password do jogador
 * @param {Date} data - data do pais do jogado
 * @param {int} telemovel - numero do telemovel do jogador
 */
function Jogador(id, name, email, telemovel, idEquipa) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.telemovel = telemovel;
    this.idEquipa = idEquipa;
};