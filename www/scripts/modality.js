"use strict";

/** 
 * @class Estrutura com capacidade de armazenar o estado de uma entidade jogador 
 * @constructs Jogador
 * @param {text} name - nome da modalidade
 * @param {text} name_type - nome do tipo
 * @param {int} type_id - id do tipo
 */
function Modality(name, name_type, type_id) {
    this.name = name;
    this.name_type = name_type;
    this.type_id = type_id;
};