"use strict";
/**
 * Função que será executada quando a página estiver toda carregada, criando a variável global "info" com um objeto Information
 * Aproveitamos ainda para solicitar ao servidor o carregamento de dados de forma assincrona(AJAX)
 * @memberof window
 * @params {Event} event - objeto que representará o evento
 */
window.onload = function (event) {
    var info = new Information();
    info.getTornament(); //buscar todos os torneios
    info.getModalities();// buscar todas as modalidades e carregar informaçao
    info.getTeamsAndPlayers();//buscar todas as equipa e jogadores, e carregar informaçao
    window.info = info;
};

function testalert(){
    var select = document.getElementById("select-modality_create");
    alert(select.options[select.selectedIndex].value);
}
/**
 * Função que substitui todos os elementos filhos de um elemento HTML por um novo elemento HTML (facilitador de DOM)
 * @param {string} id - id do elemento HTML para o qual se pretende substituir os filhos.
 * @param {HTMLElement} newSon - elemento HTML que será o novo filho.
 */
function replaceChilds(id, newSon) {
    var no = document.getElementById(id);
    while (no.hasChildNodes()) {
        no.removeChild(no.lastChild);
    }
    no.appendChild(newSon);
};

/**
 * Função que recebe um qualquer objeto e retorna dinamicamente uma linha de tabela HTML com informação relativa ao estado das suas propriedades (CRIAR TABELA DE TORNEIOS)
 * @param {Object} object - objecto do qual vamos transformar o conteudo dos seus atributos em linhas
 * @param {boolean} headerFormat - controla de o formato é cabeçalho ou linha normal
 */
function tableLine(object, headerFormat, tornamenttable) {
    var tr = document.createElement("tr");
    var tableCell = null;
    //percorrer propriedades dos torneios
    for (var property in object) {
        if ((object[property] instanceof Function) || property == "id" || property == "equipas" || property == "started" || property == "jogos")//excluir propriedades irrelevantes para tabela
            continue;
        if (headerFormat) {
            tableCell = document.createElement("th");
            tableCell.textContent = property[0].toUpperCase() + property.substr(1, property.length - 1);
        } else {
            tableCell = document.createElement("td");
            tableCell.textContent = object[property];
        }
        tr.appendChild(tableCell);
    }
    if(tornamenttable){
        //verificar se o torneio já começou, e disabilitar o crud caso já tenha começado
        var isDisabledStarted = "";
        var isDisabledNotStarted = "";
        if(object.started == 1){
            isDisabledStarted = "disabled";    
        }else{
            isDisabledNotStarted = "disabled";
        }

        var addTeamCell = document.createElement("td");
        addTeamCell.innerHTML = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTeamModal" onclick="setAddTeamModal('+ object.id +');" '+ isDisabledStarted +'>Adicionar Equipa</button>';
        
        var editTeamCell = document.createElement("td");
        editTeamCell.innerHTML = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="resetEditTeamModalInputs();setEditTornamentTeamsModalSelect('+ object.id +');setTornamentNameLabels('+ object.id +');" '+ isDisabledStarted +'>Editar Equipas</button>';
        
        var editTornament = document.createElement("td");
        editTornament.innerHTML = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-tornament-modal-lg" onclick="setEditModalSubmit('+ object.id +');fillTornamentEditInfo('+ object.id +');setTornamentNameLabels('+ object.id +');" '+ isDisabledStarted +'>Editar Torneio</button>';
        
        var editResultsCell = document.createElement("td");
        editResultsCell.innerHTML = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#resultsModal" onclick="setGamesSelect('+ object.id +');setTornamentNameLabels('+ object.id +');" '+ isDisabledNotStarted +'>Editar Resultados</button>';
        
        var tornamentTableCell = document.createElement("td");
        tornamentTableCell.innerHTML = '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#leagueTableModal" onclick="setRankigsTable('+ object.id +');setTornamentNameLabels('+ object.id +');setDeleteTournamentOnClick('+ object.id +');" '+ isDisabledNotStarted +'>Ver Tabela</button>';
        
        tr.appendChild(addTeamCell);
        tr.appendChild(editTeamCell);
        tr.appendChild(editTornament);
        tr.appendChild(editResultsCell);   
        tr.appendChild(tornamentTableCell); 
    }
    return tr;
};


function rankingsTableLine(team, rankNumber) { //construir uma linha de classificações de um torneio
        var tr = document.createElement("tr");

        var rankNumberCell = document.createElement("th");
        rankNumberCell.textContent = rankNumber;
        rankNumberCell.setAttribute("scope", "row");


        var teamNameCell = document.createElement("td");
            teamNameCell.textContent = team.nome;
         
        var pointsCell = document.createElement("td");
            pointsCell.textContent = team.pontos;

        var goalsScoredCell = document.createElement("td");
        goalsScoredCell.textContent = info.getTeamTotalGoals(team.id);

        var goalsSufferedCell = document.createElement("td");
        goalsSufferedCell.textContent = info.getTeamTotalGoalsSuffered(team.id);
       
        tr.appendChild(rankNumberCell);
        tr.appendChild(teamNameCell);
        tr.appendChild(pointsCell);
        tr.appendChild(goalsScoredCell);
        tr.appendChild(goalsSufferedCell);   
    
    return tr;
};


/**
 * crias os selects de modalidade e adiciona-os aos modais
 */
function createModalitySelects(modalities_array) {
    var first_opt = document.createElement("option");
    first_opt.textContent = "...";
    first_opt.value = "";
    replaceChilds("select-modality", first_opt);
    replaceChilds("select-modality_create", first_opt.cloneNode(true));
    var createTornamentSelect = document.getElementById("select-modality");
    var editTornamentSelect = document.getElementById("select-modality_create");
    var inserted_mod = [];
    modalities_array.forEach(function(modality) {
        if(!inserted_mod.includes(modality.name)){
            var option = document.createElement("option");
            option.textContent = modality.name;
            inserted_mod.push(modality.name);
            createTornamentSelect.appendChild(option);
            editTornamentSelect.appendChild(option.cloneNode(true));
        }
      });

      createTornamentSelect.selectedIndex = "0";
      editTornamentSelect.selectedIndex = "0";         
};

/**
 * crias os selects de tipo de torneio e adiciona-os aos modais
 */
function createModalityTypeSelects() {
    var select = document.getElementById("select-modality_create");
    var svalue= select.options[select.selectedIndex].value;

    var select_type_create = document.getElementById("select-type-create");
    
    if(svalue != ""){
        var first_opt = document.createElement("option");
        first_opt.textContent = "...";
        first_opt.value = "";
        replaceChilds("select-type-create", first_opt);
    
        info.modalities_and_types.forEach(function(modality) {
            if(modality.name == svalue){
    
                var option = document.createElement("option");
                option.textContent = modality.name_type;
    
                select_type_create.appendChild(option);
            }
          });
    
          select_type_create.disabled = false;
          select_type_create.selectedIndex = "0";
    }else{
          select_type_create.selectedIndex = "0";
          select_type_create.disabled = true;
    }
};


/**
 * crias os selects de tipo de torneio e adiciona-os aos modais EDITART TORNEIO
 */
function createModalityTypeSelectsEDIT() {
    var select = document.getElementById("select-modality");
    var svalue= select.options[select.selectedIndex].value;

    var select_type_create = document.getElementById("select-type-edit");
    
    if(svalue != ""){
        var first_opt = document.createElement("option");
        first_opt.textContent = "...";
        first_opt.value = "";
        replaceChilds("select-type-edit", first_opt);
    
        info.modalities_and_types.forEach(function(modality) {
            if(modality.name == svalue){
    
                var option = document.createElement("option");
                option.textContent = modality.name_type;
    
                select_type_create.appendChild(option);
            }
          });
    
          select_type_create.disabled = false;
          select_type_create.selectedIndex = "0";
    }else{
          select_type_create.selectedIndex = "0";
          select_type_create.disabled = true;
    }
};


/**
 * fazer set do onclick dos botoes de editar torneio
 */
function setEditModalSubmit(tornamentId){
    document.getElementById('editTornamentForm').setAttribute("action", "javascript:info.processTornament('update', "+tornamentId+");");
    document.getElementById('startTornamentButton').setAttribute("onclick", "info.startTornament("+ tornamentId +");");
}

/**
 * fazer set do onclick dos botoes de adicionar equipa torneio
 */
function setAddTeamModal(tornamentTeamId){
    document.getElementById('addTeamForm').setAttribute("action", "javascript:info.processTeam('create', "+tornamentTeamId+");");
}

/**
 * fazer set do select de editar equipas
 */
function setEditTornamentTeamsModalSelect(tornamentTeamId){

    var first_opt = document.createElement("option");
    first_opt.textContent = "Equipas...";
    first_opt.value = "";
    replaceChilds("inlineFormCustomSelect", first_opt);
 
    var teamsSelect = document.getElementById("inlineFormCustomSelect");
    var tornamentTeams = getTornamentTeams(tornamentTeamId);

    tornamentTeams.forEach(function(team) {
      
        var option = document.createElement("option");
        option.textContent = team.nome;
        option.value = team.id;
        teamsSelect.appendChild(option);
      });

      teamsSelect.selectedIndex = "0";
 }

    //constuir o select de jogadoresd consoante uma equipa
    function setSelectedTeamEdit(){
        var teamSelect = document.getElementById("inlineFormCustomSelect");
        var svalue= teamSelect.options[teamSelect.selectedIndex].value; //teamID
        var nameInput = document.getElementById('teamName');
        var playersSelect = document.getElementById('select-players');
        
        if(svalue != ""){
            let team = getTeam(svalue);
            nameInput.value = team.nome;

            resetSelect("select-players" , "Jogadores...");

            team.jogadores.forEach(function(player) {  
                var option = document.createElement("option");
                option.textContent = player.name;
                option.value = player.id;
                playersSelect.appendChild(option);
            });

            playersSelect.selectedIndex = "0";
        }else{
            resetSelect("select-players" , "Jogadores...");
            nameInput.value = "";
        }
    }

    //construir select de jogos de um torneio
    function setGamesSelect(tornamentID){
        var gamesSelect = document.getElementById("ResultsGamesSelect");
    
        let tornament = getTornamentByID(tornamentID);
        
        resetSelect("ResultsGamesSelect" , "Jogos...");
        document.getElementById('labelTeam1').textContent = 'Equipa1';
        document.getElementById('labelTeam2').textContent = 'Equipa2'; 
        document.getElementById('team1_result').value = "";
        document.getElementById('team2_result').value = "";

        tornament.jogos.forEach(function(game) {  
            var option = document.createElement("option");
            option.textContent = game.nomeEquipa1 +" VS "+ game.nomeEquipa2;
            option.value = game.id;
            gamesSelect.appendChild(option);
        });
        gamesSelect.selectedIndex = "0";
    }
    
    //construir oo onclick de editar resultados e desabilitar inputs e botao quando ja ha resultado 
    function gamesSelectOnclick(){
   
        var gamesSelect = document.getElementById("ResultsGamesSelect");
        var svalue= gamesSelect.options[gamesSelect.selectedIndex].value; //teamID
        var resultTeam1 = document.getElementById('team1_result');
        var resultTeam2 = document.getElementById('team2_result');
        var confirmButton = document.getElementById('confirmResult');
        var labelTeam1 = document.getElementById('labelTeam1');
        var labelTeam2 = document.getElementById('labelTeam2');

        if(svalue != ""){
            let game = getGameById(svalue);
            labelTeam1.textContent = game.nomeEquipa1;
            labelTeam2.textContent = game.nomeEquipa2;

            if(game.golos1 != null){
                resultTeam1.value = game.golos1;
                resultTeam2.value = game.golos2;
                confirmButton.setAttribute("disabled","true");
                resultTeam1.setAttribute("disabled","true");
                resultTeam2.setAttribute("disabled","true");
            }else{
                resultTeam1.value = "";
                resultTeam2.value = "";
                confirmButton.removeAttribute("disabled");
                resultTeam1.removeAttribute("disabled");
                resultTeam2.removeAttribute("disabled");
            }
        
        }else{
            resultTeam1.value= "";
            resultTeam2.value= "";
            labelTeam1.textContent = "Equipa1";
            labelTeam2.textContent = "Equipa2";
        }
    }

    //validar resultado e submeter com o pedido AJAX
    function confirmResult(){
        var resultTeam1 = document.getElementById('team1_result');
        var resultTeam2 = document.getElementById('team2_result');
        var gamesSelect = document.getElementById("ResultsGamesSelect");
        var svalue= gamesSelect.options[gamesSelect.selectedIndex].value; //GAMEID
        
        if(svalue != ""){   
                info.updateGameResult(svalue, resultTeam1.value, resultTeam2.value);
                resultTeam1.setAttribute("disabled", "true");
                resultTeam2.setAttribute("disabled", "true");
                document.getElementById('confirmResult').setAttribute("disabled","true");;
        }else{
            alert("Escolha um Jogo!");
        }
    }

    function setTeamSelectSelectedOption(optionValue){
        var teamsSelect = document.getElementById("inlineFormCustomSelect");
        var svalue
        for (let index = 0; index < teamsSelect.options.length; index++) {
            const element = teamsSelect.options[index];
            if(element.value == optionValue){
                teamsSelect.selectedIndex = index;
            }
        }
    }


    function setSelectedPlayerEdit(){
               
        var playersSelect = document.getElementById('select-players');
        var svalue= playersSelect.options[playersSelect.selectedIndex].value; //teamID
        var playerNameInput = document.getElementById('player_name');
        var playerPhoneInput = document.getElementById('telefone');
        var playerEmailInput = document.getElementById('inlineFormInputGroupUsername');


        if(svalue != ""){
            let player = getPlayer(svalue);

            playerNameInput.value = player.name;
            playerPhoneInput.value = player.telemovel;
            playerEmailInput.value = player.email;

        }else{
            
            playerNameInput.value = "";
            playerPhoneInput.value = "";
            playerEmailInput.value = "";
        }
    }


    function resetEditTeamModalInputs(){
        document.getElementById("editTeamForm").reset(); 
    }


    function resetSelect(elementID, firstOptText){
        var first_opt = document.createElement("option");
            first_opt.textContent = firstOptText;
            first_opt.value = "";
            replaceChilds(elementID, first_opt);
    }

    function getTeam(teamID){
        var team;
        info.teams.forEach(element => {
            if(element.id == teamID){
                team = element;
                return true;
            }
        });
        return team;
    }

    function getGameById(id){
        var game;
        info.games.forEach(element => {
            if(element.id == id){
                game = element;
                return true;
            }
        });
        return game;
    }

    function getPlayer(playerID){
        var player;
        info.players.forEach(element => {
            if(element.id == playerID){
                player = element;
                return true;
            }
        });
        return player;
    }



function getTornamentTeams(tornamentTeamId){
    var teams = [];
    info.tornaments.forEach(tor => {
        if(tor.id == tornamentTeamId){
            teams =tor.equipas;
            return true;
        }
    });
    return teams;
}


/**
 * preencher inputs com a informação atual do torneio a ser editado
 */
function fillTornamentEditInfo(tornamentId){
    var tornament_to_edit;
    info.tornaments.forEach(element => {
        if(element.id == tornamentId){
            tornament_to_edit = element;
            return true; //stop the for each
        }
    });
    
    //inputs
    var editTornamentName = document.getElementById('tornament_name');
    var editTornamentModality = document.getElementById('select-modality');
    var editTornamentType = document.getElementById('select-type-edit');
    var editTornamentDate = document.getElementById('creation_date');
    var editTornamentHour = document.getElementById('hour-time');
    var editTornamentMinute = document.getElementById('minute-time');
    var editTornamentLocation = document.getElementById('location-edit');

    //fill the text inputs
    editTornamentName.value = tornament_to_edit.nome;
    editTornamentDate.value = tornament_to_edit.data;
    editTornamentHour.value = tornament_to_edit.hora.split(':')[0];
    editTornamentMinute.value = tornament_to_edit.hora.split(':')[1];
    editTornamentLocation.value = tornament_to_edit.local;
    //fill the selects
    for(let i=0; i<editTornamentModality.options.length;i++){
        if(editTornamentModality.options[i].value == tornament_to_edit.modalidade){
            editTornamentModality.selectedIndex = i;
            //trigger the onchange event
            editTornamentModality.onchange();
            break;
        }     
     }

     for(let i=0; i<editTornamentType.options.length;i++){
        if(editTornamentType.options[i].value == tornament_to_edit.tipo){
            editTornamentType.disabled = false;
            editTornamentType.selectedIndex = i;
            break;
        }     
     }
}
   

function setTeamEditOnclick(){
    var teamSelect = document.getElementById("inlineFormCustomSelect");
    var svalue= teamSelect.options[teamSelect.selectedIndex].value; //teamID
    if(svalue != ""){
        document.getElementById('editTeamButton').setAttribute("onclick", "info.processTeam('update', "+ svalue +")");
        document.getElementById('deleteTeam').setAttribute("onclick", "info.deleteTeam("+ svalue +");");
    }else{
        document.getElementById('editTeamButton').setAttribute("onclick", "");
        document.getElementById('deleteTeam').setAttribute("onclick", "");
    }
}

function getTornamentByID(id){
    var tornament = null;
    info.tornaments.forEach(element => {
        if(element.id == id){
            tornament = element;
            return true;
        }
    });
    return tornament;
}

function setRankigsTable(tournamentID){
    var tableBody = document.getElementById("tablePreview").tBodies[0];

            //removert todos os elementos da tabela da torneios
    var child = tableBody.lastElementChild;  
    while (child) { 
        tableBody.removeChild(child); 
        child = tableBody.lastElementChild; 
     }

     var teamsTable = info.getTornamentTableArray(tournamentID);
     console.log(teamsTable);
     var rank = 1;
     teamsTable.forEach(team =>{

     tableBody.appendChild(rankingsTableLine(team, rank));
     rank++;
    });
}


function setTornamentNameLabels(tournamentID){
    var name = getTornamentByID(tournamentID).nome;
    var elements = document.getElementsByClassName('tornamentNameTeams');
    for (let index = 0; index < elements.length; index++) {
        const element = elements[index];
        element.textContent = name;
    }
}

function setDeleteTournamentOnClick(tournamentID){
    document.getElementById('deleteTornament').setAttribute("onclick", "info.deleteTornament("+ tournamentID +");");
}

function setDeleteTeamOnClick(){
    var teamSelect = document.getElementById("inlineFormCustomSelect");
    var svalue= teamSelect.options[teamSelect.selectedIndex].value; //teamID
    if(svalue != ""){
        document.getElementById('deleteTeam').setAttribute("onclick", "info.deleteTeam("+ svalue +");");
    }else{
        document.getElementById('deleteTeam').setAttribute("onclick", "");
    }   
}

function setDeletePlayerOnClick(){
    var playerSelect = document.getElementById("select-players");
    var svalue= playerSelect.options[playerSelect.selectedIndex].value; //playerID
    if(svalue != ""){
        document.getElementById('deletePlayerBtn').setAttribute("onclick", "info.deletePlayer("+ svalue +");");
    }else{
        document.getElementById('deletePlayerBtn').setAttribute("onclick", "");
    }   
}