"use strict";

/** 
 * @class Guarda toda informação necessaria na execução do exercicio 
 * @constructs Informacao
 * @property {modalities_and_types[]} modalities_and_types - 
 * @property {tornaments[]} tornaments - Array de objetos do tipo Tornament, para guardar todos os torneios do nosso sistema
 */
function Information() {
    this.modalities_and_types = [];
    this.tornaments = [];
    this.teams = [];
    this.players = [];
    this.games = [];
};



/**
 * Função que que tem como principal objetivo solicitar ao servidor NODE.JS o recurso person através do verbo GET, usando pedidos assincronos e JSON
 */
Information.prototype.getTornament = function() {
    var xht = new XMLHttpRequest();
    xht.open("GET", "/tornaments", true);
    //criar variavel do array de torneios para poder ser acedida no pedido
      //dar reset aos torneios
    this.tornaments = [];
    var tornament_array = this.tornaments;
    var this_info = this;

    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
             //get the first table body tag in the table
            var tableBody = document.getElementById("tornaments_table").tBodies[0];

            //remover todos os elementos da tabela da torneios
            var child = tableBody.lastElementChild;  
            while (child) { 
                tableBody.removeChild(child); 
                child = tableBody.lastElementChild; 
            }
             
            var response = JSON.parse(xht.responseText);
            //preencher tabela de torneios
            response.tornaments.forEach(t => tornament_array.push(new Torneio(t.id_torneio, t.nome, t.modalidade, t.data_torneio,
                 t.hora_torneio.split(":")[0]+":"+t.hora_torneio.split(":")[1], t.local_torneio, t.tipo_de_torneio, t.started)));
            
            this_info.getGames();
            document.getElementById('tornamentNumber').textContent = this_info.tornaments.length;
            tornament_array.forEach(t => tableBody.appendChild(tableLine(t, false, true)));
            
         
        }
    }
    xht.send();    
};

    

/**
 * Função que que tem como principal objetivo solicitar ao servidor NODE.JS o recurso person através do verbo GET, usando pedidos assincronos e JSON
 */
Information.prototype.getModalities = function() {
    var xht = new XMLHttpRequest();
    xht.open("GET", "/modalities", true);
    var modalities_array = this.modalities_and_types;

    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            var response = JSON.parse(xht.responseText);
            response.modalities.forEach(m => modalities_array.push(new Modality(m.modalidade, m.nome, m.id_tipo)));

            createModalitySelects(modalities_array);
           // createModalityTypeSelects(modalities_array);
        }
    }
    xht.send();
};

/**
 * Função que insere ou atualiza o recurso torneio com um pedido ao servidor NODE.JS através do verbo POST ou PUT, usando pedidos assincronos e JSON
 * @param {String} acao - controla qual a operação do CRUD queremos fazer
  */
 Information.prototype.processTornament = function (acao, tornamentId) {
    //const id = parseInt(document.getElementById('id').value);
    //Definir variaveis necessárias
    /* TODO: PROCESSAR UPDATE*/
    var name;
    var date;
    var hour;
    var minute;
    var time;
    var place;
    var name_type_select;
    var id_type_tornament;
    if(acao == "create"){
        name = document.getElementById('tornament_name_create').value;
        date = (document.getElementById('creation_date_create').value).replace("/", "-");
        hour = document.getElementById('hour-time-create').value;
        minute = document.getElementById('minute-time-create').value;
        place = document.getElementById('location-create').value;
        name_type_select = document.getElementById('select-type-create');
    }else if(acao == "update"){
        name = document.getElementById('tornament_name').value;
        date = (document.getElementById('creation_date').value).replace("/", "-");
        hour = document.getElementById('hour-time').value;
        minute = document.getElementById('minute-time').value;
        place = document.getElementById('location-edit').value;
        name_type_select = document.getElementById('select-type-edit');
    }

    time = hour+":"+minute+":00";
    const name_type = name_type_select.options[name_type_select.selectedIndex].value
    var id_type_tornament;
    this.modalities_and_types.forEach(element => {
        if(name_type == element.name_type){
            id_type_tornament =  element.type_id;
            return true;
        }
    });
    const type = id_type_tornament;

    //construir informação necessária ao torneio
    const newTornament =  {
        name: name,
        date: date,
        time: time,
        place: place,
        type_id: type
    }

    const xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    /**
     * Guardar referência para o 'this' para que possa ser utilizado nos event handlers e callbacks.
     * Assim evita-se o acesso através da referência global 'info' definida no 'main.js'.
     */
    const self = this;
    if (acao === 'create') {
        /** @todo Completar */
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                self.getTornament();
                $('#tornamentCreateModal').modal('toggle'); 
                document.getElementById("createTornamentForm").reset(); 

            }
        }
        xhr.open('POST', '/tornament');
    } else if (acao === 'update') {
        /** @todo Completar */
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                self.getTornament();
                $('#tornamentModal').modal('toggle'); 
                alert("Torneio Editado!");
                document.getElementById("editTornamentForm").reset();
            }
        }
        xhr.open('PUT', '/tornament/' + tornamentId);
    }
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(newTornament));
}


/**
 * Função que insere ou atualiza o recurso equipa com um pedido ao servidor NODE.JS através do verbo POST ou PUT, usando pedidos assincronos e JSON
 * @param {String} acao - controla qual a operação do CRUD queremos fazer
  */
 Information.prototype.processTeam = function (acao, tornamentTeamId) {
    //const id = parseInt(document.getElementById('id').value);
    //Definir variaveis necessárias
    var name;

    if(acao == "create"){
        name = document.getElementById('team_name').value;
    }else if(acao == "update"){
        name = document.getElementById('teamName').value;
    }

    var body = {name : name};
    
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    /**
     * Guardar referência para o 'this' para que possa ser utilizado nos event handlers e callbacks.
     * Assim evita-se o acesso através da referência global 'info' definida no 'main.js'.
     */
    const self = this;
    if (acao === 'create') {

        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                self.getTeamsAndPlayers();
                $('#addTeamModal').modal('toggle'); 
                alert('Equipa Criada!');
                document.getElementById("addTeamForm").reset(); 

            }
        }
        xhr.open('POST', '/team/'+ tornamentTeamId);
    } else if (acao === 'update') {
     
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                self.teams.forEach(element => {
                    if(element.id == tornamentTeamId)
                        element.nome = body.name;
                        return true;
                });
 
                self.getTeamsAndPlayers();      
                $('#exampleModal').modal('toggle');
                alert("Equipa Editada!");

            }
        }
        xhr.open('PUT', '/team/' + tornamentTeamId);
    }
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(body));
}


/**
 * Função que insere ou atualiza o recurso equipa com um pedido ao servidor NODE.JS através do verbo POST ou PUT, usando pedidos assincronos e JSON
 * @param {String} acao - controla qual a operação do CRUD queremos fazer
  */
 Information.prototype.processPlayer = function (acao) {
    //const id = parseInt(document.getElementById('id').value);
    //Definir variaveis necessárias
    var name = document.getElementById('player_name').value;
    var tlf = document.getElementById('telefone').value;
    var email = document.getElementById('inlineFormInputGroupUsername').value;

    var teamSelect = document.getElementById("inlineFormCustomSelect");
    var svalue= teamSelect.options[teamSelect.selectedIndex].value; //teamID
    var teamID = svalue;

    if(teamID == ""){
        alert('Selecione uma Equipa!');
        return false;
    }

    var playerSelect = document.getElementById("select-players");
    var pvalue= playerSelect.options[playerSelect.selectedIndex].value; //teamID
    var playerID = pvalue;

    if(acao == 'update'){
        if(playerID == ""){
            alert('Selecione um Jogador!');
            return false;   
        }     
    }

    if(!document.getElementById('player_name').checkValidity() || !document.getElementById('telefone').checkValidity() || !document.getElementById('inlineFormInputGroupUsername').checkValidity()){
        alert('Preencha os Campos Corretamente!');
        return false;
    }

    if(this.checkPlayerInfoExists(tlf, email, playerID)){      
        playerSelect.onchange();
        alert('Já existe um jogador com esse Telefone ou E-mail!');  
        return false;                            
    }      
    
    

    var player = new Jogador(playerID, name, email, tlf, teamID);

    const xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    /**
     * Guardar referência para o 'this' para que possa ser utilizado nos event handlers e callbacks.
     * Assim evita-se o acesso através da referência global 'info' definida no 'main.js'.
     */
    const self = this;
    if (acao === 'create') {

        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                //$('#addTeamModal').modal('toggle'); 
                //document.getElementById("addTeamForm").reset(); 
                self.getTeamsAndPlayers();
                var foundIt = false;
                self.tornaments.forEach(tor => {
                    if(!foundIt){
                        tor.equipas.forEach(equ => {
                            if(equ.id == teamID){
                                var playerp = new Jogador(self.getHighestPlayerID()+1, name, email, tlf, teamID);
                                equ.jogadores.push(playerp);
                                self.players.push(playerp);
                               return true;
                            }
                        });
                    }else{
                        return true;
                    }
                
                });

                resetSelect("select-players" , "Jogadores...");     
                setSelectedTeamEdit();
                playerSelect.onchange();
                alert("Jogador Criado!");
            }
        }
        xhr.open('POST', '/player');
    } else if (acao === 'update') {
     
        xhr.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                self.getTeamsAndPlayers();      
                //$('#exampleModal').modal('toggle');
                //update player
                var foundIt = false;
                self.tornaments.forEach(tor => {
                    if(!foundIt){
                        tor.equipas.forEach(equ => {
                            if(equ.id == teamID){
                                equ.jogadores.forEach(play => {
                                    if(play.id == playerID){
                                        //substituir jogador
                                        play.name = name;
                                        play.telemovel = tlf;
                                        play.email = email;
                                        foundIt = true;

                                        self.players.forEach(play => {
                                            if(play.id == playerID){
                                                 //substituir jogador no array no players
                                                play.name = name;
                                                play.telemovel = tlf;
                                                play.email = email;
                                                return true;
                                             }
                                        });
                                        return true;
                                    }
                                });
                                  return true;
                            }
                        });
                    }else{
                        return true;
                    }
                
                });
                var pIndex = playerSelect.selectedIndex;
                resetSelect("select-players" , "Jogadores...");     

 
                setSelectedTeamEdit();
                playerSelect.getElementsByTagName('option')[pIndex].selected = 'selected';
                alert("Jogador Editado!");

            }
        }
        xhr.open('PUT', '/player/' + playerID);
    }
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(player));
}


/**
 * Função que que tem como principal objetivo criar no servidor NODE.JS , os jogos de um torneio através do verbo POST, usando pedidos assincronos e JSON
 */
Information.prototype.checkPlayerInfoExists = function(playerTlf, playerEmail, playerID) {

    for (let index = 0; index < this.players.length; index++) {
        const element = this.players[index];
        if((element.telemovel == playerTlf || element.email == playerEmail) && element.id != playerID){
            return true
        }     
    }   
    return false;
};


/**
 * Função que que tem como principal objetivo criar no servidor NODE.JS , os jogos de um torneio através do verbo POST, usando pedidos assincronos e JSON
 */
Information.prototype.startTornament = function(tornamentId) {
    if(getTornamentByID(tornamentId).equipas.length < 2){
        alert("Têm de haver pelo menos 2 equipas para começar um torneio!");
        return false;
    }
    var teamsIdArray = [];

    this.tornaments.forEach(torneio => {
        if(torneio.id == tornamentId){
            torneio.equipas.forEach(equipa => {
                teamsIdArray.push(equipa.id);
            });
            return true;
        }
    });

    var xht = new XMLHttpRequest();
    xht.open("POST", "/games/"+tornamentId, true);
    var this_info = this;

    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            this_info.setTornamentStarted(tornamentId);
        }
    }
    var teams = {tornamentId: tornamentId, teams: teamsIdArray};
    xht.setRequestHeader('Content-Type', 'application/json');
    xht.send(JSON.stringify(teams));
};

/**
 * Função que que tem como principal objetivo criar no servidor NODE.JS , os jogos de um torneio através do verbo POST, usando pedidos assincronos e JSON
 */
Information.prototype.setTornamentStarted = function(tornamentId) {
   
    var xht = new XMLHttpRequest();
    xht.open("PUT", "/start_tornament/"+tornamentId, true);
    var this_info = this;
    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            this_info.getTeamsAndPlayers();
            this_info.getTornament();
            $('#tornamentModal').modal('toggle');
            alert("Torneio Iniciado!");
        }
    }
    xht.send();
};


/**
 * Função que que tem como principal objetivo ir buscar no servidor NODE.JS , as equipas e os seus jogadores através do verbo GET, usando pedidos assincronos e JSON
 */
Information.prototype.getTeamsAndPlayers = function() {

    //PEDIDO PARA PREENCHER TODAS AS EQUIPAS
    var xht = new XMLHttpRequest();
    xht.open("GET", "/teams", true);
    xht.responseType = 'json';
    var this_info = this;
      
    xht.onreadystatechange = function() {
    
        var response = xht.response;
       
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            this_info.teams = [];
            response.teams.forEach(t => {
                var team = new Equipa(t.id_equipa, t.nome, t.id_torneio, t.pontos);
                this_info.teams.push(team);        
            });
             document.getElementById('teamsNumber').textContent = this_info.teams.length;
             //JOGADORES
             this_info.getPlayers(this_info.equipas); 
        }
    }
    xht.send(); 
};

/**
 * Função auxiliar à getTeamsAndPlayers
 */
Information.prototype.getPlayers = function() {
    var xht = new XMLHttpRequest();
    xht.open("GET", "/players", true);
    xht.responseType = 'json';

    var this_info = this;
    xht.onreadystatechange = function() {

        var response = xht.response;

        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {      
                this_info.players = [];                               
                response.players.forEach(j => this_info.players.push(new Jogador(j.id_jogador, j.nome, j.email,  j.nrm_telemovel, j.id_equipa)));

                document.getElementById('playersNumber').textContent = this_info.players.length;

                this_info.players.forEach(jogador => {
                    this_info.teams.forEach(equipa => {

                        if (equipa.id == jogador.idEquipa) {
                            equipa.jogadores.push(jogador);
                        }
                    });
                });    
                
                this_info.tornaments.forEach(torneio => {
                    torneio.equipas = [];
                    this_info.teams.forEach(equipa => {

                        if (equipa.idTorneio == torneio.id) {
                            torneio.equipas.push(equipa);
                        }
                    });
                });        
        }
    }
    xht.send(); 
}

/**
 * Função para ir buscar todos os jogos existêntes à BD
 */
Information.prototype.getGames = function() {
    var xht = new XMLHttpRequest();
    xht.open("GET", "/games", true);
    xht.responseType = 'json';

    var this_info = this;
    xht.onreadystatechange = function() {

        var response = xht.response;

        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {      
                this_info.games = [];               
                //carregar jogos existentes
                response.games.forEach(g => this_info.games.push(new Jogo(g.id_jogo, g.id_equipa1, g.id_equipa2,
                             g.golos_equipa1, g.golos_equipa2, g.id_torneio, g.nome_equipa1, g.nome_equipa2)));

                document.getElementById('gamesNumber').textContent = this_info.games.length;
                //preencher jogos de cada torneio
                this_info.tornaments.forEach(torn => {
                    this_info.games.forEach(game => {
                        if(torn.id == game.idTorneio){
                             torn.jogos.push(game);
                        }
                    });
                            
                });     
        }
    }
    xht.send(); 
}


/**
 * Função para ir buscar todos os jogos existêntes à BD
 */
Information.prototype.updateGameResult = function(gameID, goalsTeam1, goalsTeam2) {
    var xht = new XMLHttpRequest();
    xht.open("PUT", "/games/"+gameID, true);
    xht.responseType = 'json';
            
    var game;
    var this_info = this;
    this_info.games.forEach(element => {
        if(element.id == gameID){
            game = element;
            return true;
        }
    });

  
    xht.onreadystatechange = function() {

        var response = xht.response;

        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {      
                             
                //carregar jogos existentes
    


                game.golos1 = goalsTeam1;
                game.golos2 = goalsTeam2;
                alert("Resultado Submetido!");     
        }
    }
    xht.setRequestHeader('Content-Type', 'application/json');
    xht.send(JSON.stringify({goals1: goalsTeam1, goals2: goalsTeam2, idteam1: game.idEquipa1, idteam2: game.idEquipa2})); 
}


/**
 * Função que que tem como principal objetivo ir buscar no servidor NODE.JS , as equipas e os seus jogadores através do verbo GET, usando pedidos assincronos e JSON
 */
Information.prototype.getHighestPlayerID = function() {
    var id = -1;
    this.players.forEach(element => {
        if(element.id > id){
            id = element.id;
        }
    });
    return id;
};

/**
 * Função que que tem como principal objetivo devolver as equipas de um torneio, ordenadas por pontos
 */
Information.prototype.getTornamentTableArray = function(tornamentID) {
       var tornamentTable;
        this.tornaments.forEach(tournament => {
           if(tournament.id == tornamentID){
            
             tornamentTable = tournament.equipas.slice(0);
        
            return true;
           } 
        });
       
        tornamentTable.sort(function(a, b) {
            
            return (a.pontos) < (b.pontos);
        });

        return tornamentTable;
};

/**
 * Função que que tem como principal devolver o total de golos marcados por uma equipa
 */
Information.prototype.getTeamTotalGoals = function(teamID) {
    var totalGoals = 0;
     this.games.forEach(game => {

        if(game.idEquipa1 == teamID && game.golos1 != null){ 
            totalGoals += parseInt(game.golos1);  
        }else if(game.idEquipa2 == teamID && game.golos2 != null){
            totalGoals += parseInt(game.golos2);
        }
     });
    return totalGoals;
   
};

/**
 * Função que que tem como principal devolver o total de golos sofridos por uma equipa
 */
Information.prototype.getTeamTotalGoalsSuffered = function(teamID) {
    var totalGoals = 0;
     this.games.forEach(game => {
        if(game.idEquipa1 == teamID && game.golos2 != null){ 
            totalGoals += parseInt(game.golos2);  
        }else if(game.idEquipa2 == teamID && game.golos1 != null){
            totalGoals += parseInt(game.golos1);
        }
     });
     console.log(totalGoals);
    return totalGoals;
   
};


//DELETES 
/**
 * Função que que tem como principal objetivo eliminar um torneio , equipas, os seus jogos e jogadores
 */
Information.prototype.deleteTornament = function(tournamentID) {
    var xht = new XMLHttpRequest();
    xht.open("DELETE", "/tornament/"+tournamentID, true);
    var this_info = this;
    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            //var response = JSON.parse(xht.responseText);
            this_info.getTornament();
            this_info.getTeamsAndPlayers();
            this_info.getGames();
            alert('Torneio Eliminado!');
        }
    }
    xht.send();
};


/**
 * Função que que tem como principal objetivo eliminar uma equipa e os seus jogadores
 */
Information.prototype.deleteTeam = function(teamID) {
    var xht = new XMLHttpRequest();
    xht.open("DELETE", "/teams/"+teamID, true);
    var this_info = this;
    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            //var response = JSON.parse(xht.responseText);
            this_info.getTornament();
            this_info.getTeamsAndPlayers();
            this_info.getGames();  
            $('#exampleModal').modal('toggle');
            alert('Equipa Eliminada!');
        }
    }
    xht.send();
};

/**
 * Função que que tem como principal objetivo eliminar um jogador
 */
Information.prototype.deletePlayer = function(playerID) {
    var playerSelect = document.getElementById("select-players");
    var pvalue= playerSelect.options[playerSelect.selectedIndex].value; //playerID

    if(pvalue == ""){
        alert('Selecione um Jogador!');
        return false;
    }
    var xht = new XMLHttpRequest();
    xht.open("DELETE", "/players/"+playerID, true);
    var this_info = this;
    xht.onreadystatechange = function() {
        if ((this.readyState == XMLHttpRequest.DONE) && (this.status == 200)) {
            
            //take the player out of the arrays, GLOBAL PLAYERS and PLAYER IN TEAM
            this_info.getTeamsAndPlayers();
            for (let index = 0; index < this_info.players.length; index++) {
                const element = this_info.players[index];
                if(element.id == playerID){
                    this_info.players.splice(index,1);
                    break;
                }          
            }
            var foundit = false;
            this_info.tornaments.forEach(tour => {
                if(!foundit){

             
                    tour.equipas.forEach(equ => {
                        if(!foundit){
                            for (let index = 0; index < equ.jogadores.length; index++) {
                                const element = equ.jogadores[index];
                                if(element.id == playerID){
                                    equ.jogadores.splice(index,1);
                                    foundit = true;
                                    break;
                                }          
                            }
                        }                        
                    });
                }
            });

            resetSelect("select-players" , "Jogadores...");     
            setSelectedTeamEdit();
            playerSelect.onchange();
            alert("Jogador Eliminado!");
        }
    }
    xht.send();
};
