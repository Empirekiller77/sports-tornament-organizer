"use strict";
const mysql = require("mysql");
const options = require("./connection-options.json");
const robin = require('roundrobin');
/**
 * Função para retornar a lista de torneios da BD.
 * @param {*} req 
 * @param {*} res 
 */
function getTornaments(req, res) {
    //round robin test
   /* var ee = robin(6, [1, 2, 3, 4, 5, 6]);
    var valuesStringArray = [];
    ee.forEach(element => {
        element.forEach(e2 => {
            let test = "("+e2.toString()+","+ 2 +")";
            valuesStringArray.push(test);
        });    
    });
    console.log("values "+valuesStringArray.toString());
    */
    var connection = mysql.createConnection(options);
    connection.connect(function(err){
        if (err){
        return console.error('error: ' + err.message);
    }
});
    var query = "SELECT torneio.id_torneio, torneio.nome, modalidade.nome as modalidade, DATE_FORMAT(torneio.data_torneio, '%Y-%m-%d') as data_torneio, torneio.hora_torneio, torneio.local_torneio, tipodetorneio.nome as tipo_de_torneio, torneio.started FROM teamup.torneio INNER JOIN tipodetorneio, modalidade where torneio.id_tipo_torneio = tipodetorneio.id_tipo_torneio and tipodetorneio.id_modalidade = modalidade.id_modalidade ORDER BY torneio.data_torneio DESC";
      connection.query(query, function(err, rows) {
        if (err) {
            res.json({ "message": "Error", "error": err });
            console.log("error in db!!!");
        } else {
            res.json({ "message": "Success", "tornaments": rows });
        }
    });
}
module.exports.getTornaments = getTornaments;

/**
 * Função para retornar a criar ou atualizar torneios da BD.
 * @param {*} req 
 * @param {*} res 
 */
function createUpdateTornament(req, res) {
    var connection = mysql.createConnection(options);

    let name = req.body.name;
    let date = req.body.date;
    let time = req.body.time;
    let place = req.body.place;
    let type_id = req.body.type_id;

    let sql = (req.method === 'PUT') ? "UPDATE torneio SET torneio.nome = ?, torneio.data_torneio= ?, torneio.hora_torneio= ?, torneio.local_torneio= ?, torneio.id_tipo_torneio= ? WHERE torneio.id_torneio = ?" : 
    "INSERT INTO torneio (torneio.nome, torneio.data_torneio, torneio.hora_torneio, torneio.local_torneio, torneio.id_tipo_torneio) VALUES (?, ?, ?, ?, ?)";
    
    connection.connect(function (err) {
        if (err) throw err;
        connection.query(sql, [name, date, time, place, type_id, req.params.id], function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.createUpdateTornament = createUpdateTornament;

/**
 * Função para retornar a lista de torneios da BD.
 * @param {*} req 
 * @param {*} res 
 */
function getTornamentTypes(req, res) {
    var connection = mysql.createConnection(options);
    connection.connect(function(err){
        if (err){
        return console.error('error: ' + err.message);
    }
});
    var query = "SELECT modalidade.nome as modalidade, tipodetorneio.nome, tipodetorneio.id_tipo_torneio as id_tipo FROM modalidade INNER JOIN tipodetorneio where tipodetorneio.id_modalidade = modalidade.id_modalidade;";
      connection.query(query, function(err, rows) {
        if (err) {
            res.json({ "message": "Error", "error": err });
            console.log("error in db!!!");
        } else {
            res.json({ "message": "Success", "modalities": rows });
        }
    });
}
module.exports.getTornamentTypes = getTornamentTypes;

/**
 * Função para criar ou atualizar equipas da BD.
 * @param {*} req 
 * @param {*} res 
 */
function createUpdateTeam(req, res) {
    var connection = mysql.createConnection(options);

    let name = req.body.name;
    
    let sql = (req.method === 'PUT') ? "UPDATE equipa SET equipa.nome = ? WHERE equipa.id_equipa = ?" : 
    "INSERT INTO equipa (equipa.nome, equipa.id_torneio) VALUES (?, ?)";
    
    connection.connect(function (err) {
        if (err) throw err;
                
        connection.query(sql, [name, req.params.tornamentTeamId], function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.createUpdateTeam = createUpdateTeam;


/**
 * Função para ir buscar uma equipa da BD.
 * @param {*} req 
 * @param {*} res 
 */
function getTeams(req, res) {
    var connection = mysql.createConnection(options);
    
    let sql = "SELECT equipa.id_equipa, equipa.nome, equipa.id_torneio, equipa.pontos FROM equipa";
    
    connection.connect(function (err) {
        if (err) throw err;
                
        connection.query(sql, function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                //console.log(rows);
                res.json({ "message": "Success", "teams": rows });
            }
        });
    });
}
module.exports.getTeams = getTeams;

/*
**
 * Função para ir buscar uma equipa da BD.
 * @param {*} req 
 * @param {*} res 
 */
function getPlayers(req, res) {
    var connection = mysql.createConnection(options);
    
    let sql = "SELECT jogador.id_jogador, jogador.nome, jogador.email, jogador.nrm_telemovel, jogador.id_equipa FROM jogador";
    
    connection.connect(function (err) {
        if (err) throw err;
                
        connection.query(sql, function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.json({ "message": "Success", "players": rows });
            }
        });
    });
}
module.exports.getPlayers = getPlayers;



/**
 * Função para criar todos os jogos round robin de um torneio.
 * @param {*} req 
 * @param {*} res 
 */
function createTornamentGames(req, res) {
    var connection = mysql.createConnection(options);
    let teamsArray = req.body.teams; //teamsIDS
    let tornamentId = req.body.tornamentId;

    var roundRobin = robin(teamsArray.length, teamsArray);
    var valuesStringArray = [];
    roundRobin.forEach(element => {
        element.forEach(e2 => {
            let test = "("+e2.toString()+","+ req.params.tornamentID +")";
            valuesStringArray.push(test);
        });    
        
    });

    var connection = mysql.createConnection(options);

    let sql = "INSERT INTO jogo (jogo.id_equipa1, jogo.id_equipa2, jogo.id_torneio) VALUES "+valuesStringArray.toString();
    
    connection.connect(function (err) {
        if (err) throw err;
                
        connection.query(sql, [req.params.tornamentID], function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.createTornamentGames = createTornamentGames;


/**
 * Função para retornar a criar ou atualizar torneios da BD.
 * @param {*} req 
 * @param {*} res 
 */
function createUpdatePlayer(req, res) {
    var connection = mysql.createConnection(options);

    let name = req.body.name;
    let email = req.body.email;
    let nrm_telemovel = req.body.telemovel;
    let id_equipa = req.body.idEquipa;

    let sql = (req.method === 'PUT') ? "UPDATE jogador SET jogador.nome = ?, jogador.email= ?, jogador.nrm_telemovel= ?, jogador.id_equipa= ? WHERE jogador.id_jogador = ?" : 
    "INSERT INTO jogador (jogador.nome, jogador.email, jogador.nrm_telemovel, jogador.id_equipa) VALUES (?, ?, ? ,?)";
    
    connection.connect(function (err) {
        if (err) throw err;
        connection.query(sql, [name, email, nrm_telemovel, id_equipa, req.params.id], function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.createUpdatePlayer = createUpdatePlayer;


/**
 * Função para alterar o atributo que indica que um torneio começou.
 * @param {*} req 
 * @param {*} res 
 */
function startTornament(req, res) {
    var connection = mysql.createConnection(options);

    let sql = "UPDATE torneio SET torneio.started = 1 WHERE torneio.id_torneio = ?";
    connection.connect(function (err) {
        if (err) throw err;
        connection.query(sql, [req.params.id], function (err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.startTornament = startTornament;


/**
 * Função para retornar a lista de jogos da BD.
 * @param {*} req 
 * @param {*} res 
 */
function getGames(req, res) {

    var connection = mysql.createConnection(options);
    connection.connect(function(err){
        if (err){
        return console.error('error: ' + err.message);
    }
});
    var query = "SELECT jogo.*, T1.nome as nome_equipa1, T2.nome as nome_equipa2 FROM jogo, equipa as T1, equipa as T2 WHERE jogo.id_equipa1 = T1.id_equipa AND  jogo.id_equipa2 = T2.id_equipa";
      connection.query(query, function(err, rows) {
        if (err) {
            res.json({ "message": "Error", "error": err });
        } else {
            res.json({ "message": "Success", "games": rows });
        }
    });
}
module.exports.getGames = getGames;


/**
 * Função para atribuir um resultado a um jogo da BD.
 * @param {*} req 
 * @param {*} res 
 */
function setGameResult(req, res) {

    var connection = mysql.createConnection(options);
    var goals1 = req.body.goals1;
    var goals2 = req.body.goals2;
    var idTeam1 = req.body.idteam1;
    var idTeam2 = req.body.idteam2;


    connection.connect(function(err){
        if (err){
        return console.error('error: ' + err.message);
    }
});
    var query = "UPDATE  jogo SET jogo.golos_equipa1 = ?, jogo.golos_equipa2 = ? WHERE jogo.id_jogo = ?";
      connection.query(query, [goals1, goals2, req.params.gameID], function(err, rows) {
        if (err) {
            res.json({ "message": "Error", "error": err });
            console.log(err);
        } else {
            var pointsQuery = "UPDATE equipa SET equipa.pontos = equipa.pontos+3 WHERE equipa.id_equipa = ?";
            var pointsparam;
            if(goals1 > goals2){
                pointsparam = [idTeam1];
            }else if(goals2 > goals1){
                pointsparam = [idTeam2];
            }else{
                pointsQuery = "UPDATE equipa as t1, equipa as t2 SET t1.pontos = t1.pontos+1, t2.pontos=t2.pontos+1 WHERE t1.id_equipa = ? and t2.id_equipa = ?";
                pointsparam = [idTeam1, idTeam2];
            }
            console.log(goals1 + " "+ goals2);
            connection.query(pointsQuery, pointsparam, function(err, rows) {
                if (err) {
                    res.json({ "message": "Error", "error": err });
                    console.log(err);
                } else {
               
                    console.log("points where inserted");
                }
            });

            res.json({ "message": "Success", "game": rows });
        }
    });
}
module.exports.setGameResult = setGameResult;


//DELETES
function deleteTornament(req, res) {
    var connection = mysql.createConnection(options);

    let sql = "DELETE FROM torneio where id_torneio = ?";

    connection.connect(function(err) {
        if (err) throw err;

        connection.query(sql, [req.params.tornamentID], function(err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.deleteTornament = deleteTornament;

function deleteGames(req, res) {
    var connection = mysql.createConnection(options);

    let sql = "DELETE FROM jogo where jogo.id_torneio = ?";

    connection.connect(function(err) {
        if (err) throw err;

        connection.query(sql, [req.params.tournamentID], function(err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.deleteGames = deleteGames;

function deleteTeam(req, res) {
    var connection = mysql.createConnection(options);

    let sql = "DELETE FROM equipa where id_equipa = ?";

    connection.connect(function(err) {
        if (err) throw err;

        connection.query(sql, [req.params.teamID], function(err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.deleteTeam = deleteTeam;

function deletePlayer(req, res) {
    var connection = mysql.createConnection(options);

    let sql = "DELETE FROM jogador where id_jogador = ?";

    connection.connect(function(err) {
        if (err) throw err;

        connection.query(sql, [req.params.playerID], function(err, rows) {
            if (err) {
                res.sendStatus(500);
                console.log(err);
            } else {
                res.send(rows);
            }
        });
    });
}
module.exports.deletePlayer = deletePlayer;