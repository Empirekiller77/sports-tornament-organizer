use teamup;

DELETE FROM Jogador;
DELETE FROM Jogo;
DELETE FROM Equipa;
DELETE FROM Torneio;
DELETE FROM TipoDeTorneio;
DELETE FROM Modalidade;

INSERT INTO modalidade
VALUES (1, 'Futebol'), (2, 'Tenis');

INSERT INTO tipodetorneio
VALUES (1, '5V5', 1),
		(2, '7V7', 1),
		(3, '11V11', 1),
		(4, '1V1', 2);
   
   -- DUMMY CONTENT BELOW
Insert into torneio (torneio.id_torneio, torneio.nome, torneio.data_torneio, torneio.hora_torneio, torneio.local_torneio, torneio.id_tipo_torneio)
values (1, 'SLB V FCP', '2019-07-15', '19:30:00', 'Estadio da Luz', 1),
		(2, 'SLB V SCP', '2019-07-20', '19:30:00', 'Estadio Jose Alvalade', 1),
        (3, 'SLB V SCP', '2019-07-25', '19:30:00', 'Estadio do Dragao', 4);

insert into equipa (equipa.id_equipa, equipa.nome, equipa.id_torneio)
values 	(1, 'SLB', 2),
		(2, 'FCP', 1),
        (3, 'SCP', 3),
        (4, 'SCP2', 2),
        (5, 'SCP3', 2),
        (6, 'SCP4', 2);
        


insert into jogador
values (1, 'Jonas', 'jonas_slb@gmail.com', 912345678, 1);
insert into jogador
values (2, 'Sequeira', 'sequeira_slb@gmail.com', 912345679, 1);
insert into jogador
values (3, 'Dias', 'dias_slb@gmail.com', 912345678, 1);
insert into jogador
values (4, 'Filipe', 'filipe_slb@gmail.com', 912345688, 1);
insert into jogador
values (5, 'Salvio', 'salvio_slb@gmail.com', 912345778, 1);

insert into jogador
values (6, 'Casillas', 'casillas_fcp@gmail.com', 912345878, 2);
insert into jogador
values (7, 'Herrera', 'herrera_fcp@gmail.com', 912345578, 2);
insert into jogador
values (8, 'Marega', 'marega_fcp@gmail.com', 912343678, 2);
insert into jogador
values (9, 'Militao', 'militao_fcp@gmail.com', 912545678, 2);
insert into jogador
values (10, 'Brahimi', 'brahimi_fcp@gmail.com', 912345698, 2);

insert into jogador
values (11, 'CasillasSCP', 'casillas_fcp@gmail.com', 912345878, 3);
insert into jogador
values (12, 'HerreraSCP', 'herrera_fcp@gmail.com', 912345578, 3);
insert into jogador
values (13, 'MaregaSCP', 'marega_fcp@gmail.com', 912343678, 3);
insert into jogador
values (14, 'MilitaoSCP', 'militao_fcp@gmail.com', 912545678, 3);
insert into jogador
values (15, 'BrahimiSCP', 'brahimi_fcp@gmail.com', 912345698, 3);
