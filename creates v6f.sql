-- create database teamUp; SE N EXISTIR
USE teamUP;

DROP TABLE IF EXISTS Jogador_Equipa;
DROP TABLE IF EXISTS Jogador;
DROP TABLE IF EXISTS Equipa_Jogo;
DROP TABLE IF EXISTS Jogo;
DROP TABLE IF EXISTS Equipa;
DROP TABLE IF EXISTS Torneio;
DROP TABLE IF EXISTS TipoDeTorneio;
DROP TABLE IF EXISTS Modalidade;





CREATE TABLE  Modalidade(
id_modalidade INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome TEXT);

CREATE TABLE  TipoDeTorneio(
id_tipo_torneio INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome TEXT,
id_modalidade INT,
FOREIGN KEY(id_modalidade) REFERENCES Modalidade(id_modalidade));

CREATE TABLE Torneio(
id_torneio INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome TEXT,
data_torneio DATE,
hora_torneio TIME,
local_torneio TEXT,
id_tipo_torneio int,
started BOOL NOT NULL DEFAULT 0,
FOREIGN KEY(id_tipo_torneio) REFERENCES TipoDeTorneio(id_tipo_torneio));

CREATE TABLE  Equipa(
id_equipa INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nome TEXT,
id_torneio INT,
pontos INT NUll DEFAULT 0,
FOREIGN KEY(id_torneio) REFERENCES Torneio(id_torneio) ON DELETE CASCADE);

CREATE TABLE Jogador (
    id_jogador INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome TEXT,
    email TEXT,
    nrm_telemovel NUMERIC(9),
    id_equipa INT NOT NULL,
    FOREIGN KEY(id_equipa) REFERENCES Equipa(id_equipa) ON DELETE CASCADE
);

CREATE TABLE  Jogo(
id_jogo INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
id_equipa1 INT,
id_equipa2 INT,
golos_equipa1 INT NULL DEFAULT NULL,
golos_equipa2 INT NULL DEFAULT NULL,
id_torneio INT,
FOREIGN KEY(id_torneio) REFERENCES Torneio(id_torneio) ON DELETE CASCADE,
FOREIGN KEY(id_equipa1) REFERENCES Equipa(id_equipa) ON DELETE CASCADE,
FOREIGN KEY(id_equipa2) REFERENCES Equipa(id_equipa) ON DELETE CASCADE);